import express from "express";
import mongoose from "mongoose";
import path from 'path';
import cookieParser from "cookie-parser";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { nextTick } from "process";
const app = express();

mongoose.connect("mongodb://127.0.0.1:27017",{
    dbName: "backend",
}).then(()=> console.log("Database Connected"))  


const user = new mongoose.Schema({
    name: String,
    email: String,
    password: String,

});

const User = mongoose.model("User",user);

const isAuthenticated = async(req,res,next) =>{
    const { token } = req.cookies;
    if(token){
        const decoded = jwt.verify(token, "qwert");
        req.user = await User.findById(decoded._id);

        next();
    }else{
        res.render("login");
    }

}
app.use(express.static(path.join(path.resolve(),"public")));
app.set("view engine", "ejs");
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
const port = 5000;
 
app.get("/add",(req,res)=>{
    Message.create({name: "abhi", email: "test@123"}).then(()=>{
        res.send("nice");
    })
})

const users = [];
app.get("/", isAuthenticated, (req,res) => {
    //res.render("index", {"name":"Devesh"});
    //console.log(res.cookies.token);
    res.render("logout", {name: req.user.name});
    
}); 
app.get("/success",(req,res) =>{
    res.render("success");
})
app.get("/users", (req,res) =>{
    res.json({
        users
    });
})
app.get("/register",(req,res) =>{
    res.render("register");
})
app.get("/logout",(req,res) => {
    res.cookie("token", undefined ,{
        httpOnly: true,
        expires: new Date(Date.now()),
    });
    res.redirect("/");
})
app.post("/contact",async (req,res) => {
    const mesageData = { name: req.body.name, email: req.body.email }
    
    console.log(mesageData);
    await Message.create(mesageData); 
    res.redirect("success");
});
app.post("/login", async (req,res) =>{
    const {name,email, password} = req.body;
    const user = await User.findOne({email});
    const isMatch = await bcrypt.compare(password, user.password);

    if(isMatch){
        const token = jwt.sign({_id: user._id}, "qwert");
        res.cookie("token", token,{
            httpOnly: true,
            expires: new Date(Date.now() + 60 * 1000)
        });
        res.redirect("/");
    }else{
        res.status(404).json({
            status: 404,
            errorMessage: "user not exist",
        })
    }
});

app.post("/register", async(req,res) =>{
    const {name, email, password} = req.body;
    let user = await User.findOne({email});
    console.log(user);
    if(user){
        res.status(404).json({
            success:false,
            message: "User already exist"
        });
    }else{
        const hashedPassword = await bcrypt.hash(password, 10);

        const user = await User.create({
            name,
            email,
            password: hashedPassword,
        });
        const token = jwt.sign({_id: user._id}, "qwert");
        res.cookie("token", token,{
            httpOnly: true,
            expires: new Date(Date.now() + 60 * 1000)
        });
        res.redirect("/");
    }
})
app.get("/api/contacts", (req,res) => {
    //res.send("get all contacts");

    res.status(404).json({
        success:true,
        contacts:[]
    });
});
 
app.listen(port, () => {
    console.log("server Running");
}); 